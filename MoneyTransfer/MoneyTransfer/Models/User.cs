﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace MoneyTransfer.Models
{
    public class User : IdentityUser
    {
        public int Identifier { get; set; }

        public int Balance { get; set; }

        public User()
        {
            Balance = 1000;
        }
    }
}
