﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.Models
{
    public class Transaction
    {
        public int Id { get; set; }

        public DateTime DateTime { get; set; }
        public int Sum { get; set; }

        public string Sender { get; set; }
        //public int SenderId { get; set; }

        public string Receiver { get; set; }
        //public int ReceiverId { get; set; }
        
    }
}
