﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoneyTransfer.Models;

namespace MoneyTransfer.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationContext _context;
        private UserManager<User> _userManager;

        public HomeController(ApplicationContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;

        }

        public async Task<IActionResult> Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                User user = await _userManager.FindByNameAsync(User.Identity.Name);
                ViewBag.balance = user.Balance;
                
            }

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Transaction(Transaction transaction)
        {
            User sender = await _userManager.FindByNameAsync(User.Identity.Name);
            if (sender.Balance < transaction.Sum)
            {
                return Json("Баланс меньше требуемой суммы!");
            }
            else
            {
                User receiver = await _userManager.FindByNameAsync(transaction.Receiver);


                sender.Balance -= transaction.Sum;
                receiver.Balance += transaction.Sum;

                transaction.Sender = sender.UserName;
                transaction.DateTime = DateTime.Now;

                _context.Users.Update(sender);
                _context.Users.Update(receiver);
                _context.Transactions.Add(transaction);
                _context.SaveChanges();

                return RedirectToAction("Index");
            }
        }
    

        public async Task<IActionResult> Alltransactions()
        {
            User user = await _userManager.FindByNameAsync(User.Identity.Name);
            ViewBag.balance = user.Balance;
            List<Transaction> transactions = _context.Transactions.Where(c => c.Sender == user.UserName || c.Receiver == user.UserName).ToList();   //Include(c => c.Sender == user.UserName).ToList();
            return View(transactions);
        }

        public string GetCulture()
        {
            return $"CurrentCulture:{CultureInfo.CurrentCulture.Name}, CurrentUICulture:{CultureInfo.CurrentUICulture.Name}";
        }
    }
}
